#+TITLE: My use of Guix
#+DESCRIPTION: Site personnel.
#+AUTHOR: Arnaud Daby-Seesaram
#+EMAIL: ds-ac@nanein.fr
#+OPTIONS: html-postamble:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://orgmode.org/worg/style/worg.css"/>
#+HTML_HEAD: <style>p { text-align: justify; }</style>
#+HTML_HEAD: <style>summary { color: green; }</style>
#+SEQ_TODO: todo | in_progress | done

#+begin_example
Note that I am still quite new to Guix.  Actually, I am new to: Guix, Guile
Scheme and Emacs...
The purpose of this page is not to teach, and it is not a tutorial either. It
simply relates my journey with Guix and lists a few things I have done or
learnt.
#+end_example

* Static Website generation from Org files.

[[https://nanein.fr][My website]] is written entirely of Org files, which can be found in [[https://gitlab.crans.org/myguix/static-nanein][this]] Git
repository.  This section explains how I got from the aforementioned Git
repository to the website you are currently browsing.

#+begin_quote
This section describes a package that translates a tree of files into a
website. It is a good opportunity for me to discover Guix build
systems.
#+end_quote

This is done in two steps:
1. write a package which has the same structure as the Git repository containing
   Org files,
2. install the package to the Nginx VM (through its system configuration),
3. add the path of the package in the Guix store as root location for the
   website of [[https://nanein.fr][nanein.fr]] (/i.e./ in the configuration of Nginx).

** Definition of the package.

*** Source of the package.

In our case, the source of the Guix package is the Git repository containing our
Org-files.

Assume in this example that :
- ~%url~ contains the url of the Git repository (~string~),
- ~%commit~ contains a commitish (~string~),
- ~%version~ contains the version of the package (~string~),
- ~%hash~ contains the hash of the repository (~string~).

#+begin_src scheme
  (origin
   (method git-fetch)
   (uri (git-reference
	 (url %url)
	 (commit %commit)))
   (file-name (git-file-name name %version))
   (sha256 (base32 %hash)))
#+end_src

*** Inputs of the package.

The package only requires ~emacs-org~ to translate the Org files. Every other
file is simply copied, and do not require any treatment.

#+begin_src scheme
  (inputs (list emacs-org emacs))
#+end_src

*** todo Build System of the package.

As one of my goals while writing this package was to discover how to use Guix
[[https://guix.gnu.org/manual/en/html_node/Build-Systems.html#Build-Systems][build systems]], I have decided to use the simplest one: the trivial build system.
In this build system, it is possible to provide a Guile script that fills the
output directory of the package, using the package input.

The scipt:
1. creates a directory ~src~,
2. recursively copies the content of the source Git repository in ~src~,
3. walks through the source tree and:
   - translate Org files to html files,
   - simply copy other files.

**** Translation of a single Org file.
#+begin_src scheme
  (define (translate-one-file filename)
    (system
#+end_src

#+begin_src scheme
  a
#+end_src

*** in_progress Final result.

@@html:<details>
<summary>Click here to read the definition of the whole package.</summary>@@
#+begin_src scheme
  (let ((commit "bcacba480b961a449996944457ee1c971a2687d7")
	(revision "2")
	(%version "0.0.1")
	(hash "1d5ypk3d5almjbd2ywhkysf6w04dm9m96d7rhxbzcwn7c339qwzi"))

    (package
     (name "static-nanein-website")
     (version (git-version %version revision commit))

     (source (origin
	      (method git-fetch)
	      (uri (git-reference
		    (url "https://gitlab.crans.org/myguix/static-nanein")
		    (commit commit)))
	      (file-name (git-file-name name version))
	      (sha256
	       (base32 hash))))

     (inputs (list emacs-org emacs coreutils))
     (build-system trivial-build-system)

     (arguments
      (list
       #:modules `((guix build utils))

       #:builder
       #~(begin
	   (use-modules (guix build utils))
	   (use-modules (ice-9 ftw)
			(ice-9 string-fun)
			(ice-9 match))

	   (define (translate-one-file filename)
	     (system
	      (string-append
	       (assoc-ref %build-inputs "emacs") "/bin/emacs "
	       filename
	       " --batch -f org-html-export-to-html --kill")))

	   (copy-recursively #+source "src")
	   (chdir "src")

	   (define (walk src-path out-path)
	     (match (stat:type (stat src-path))
	       ('directory
		(begin
		  (for-each
		   (lambda (child)
		     (walk
		      (string-append src-path "/" child)
		      (string-append out-path "/" child)))
		   (scandir src-path
			    (lambda (s)
			      (and
			       (not (string=? s "."))
			       (not (string=? s ".."))))))))

	       ('regular
		(let ((out-res-path ; directory containing the output file.
		       (string-join
			(reverse (cdr (reverse (string-split out-path
							     #\/))))
			"/")))
		  (if (eq? (string-suffix-length src-path ".org") 4)
		      ;; The file is ideed an Org file
		      (let ((src-res-path ; file.org => file.html
			     (string-append (string-drop-right src-path 3)
					    "html"))
			    (begin
			      ;; Translate the file.
			      (translate-one-file src-path)
			      ;; Copy the result...
			      (install-file src-res-path
					    out-res-path)))

			;; The file is *not* an Org file.
			(install-file src-path out-res-path))))

		(_ #t)))

	     (walk (getcwd) %output))))

      (synopsis "My static website for Nanein.")
      (description "This package defines a static website.")
      (home-page "https://nanein.fr/")
      (license gpl3+)))
#+end_src
@@html:</details>@@
